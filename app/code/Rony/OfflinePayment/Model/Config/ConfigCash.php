<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\OfflinePayment\Model\Config;

use Magento\Payment\Gateway\Config\Config;

/**
 * Class ConfigCash
 * @package Rony\OfflinePayment\Model\Config
 */
class ConfigCash extends Config
{}
