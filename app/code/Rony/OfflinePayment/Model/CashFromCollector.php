<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\OfflinePayment\Model;

use Magento\Payment\Model\Method\Adapter;

/**
 * Class CashFromCollector
 * @package Rony\OfflinePayment\Model
 */
class CashFromCollector extends Adapter
{
    /**
     * CashFromCollector Payment Code
     */
    const PAYMENT_METHOD_CASHFROMCOLLECTOR_CODE = 'cashfromcollector';
}
