<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\DevApi\Model;

use Rony\DevApi\Api\CreatePostInterface;
use Rony\DevApi\Model\Authentication\TokenBased;
use Rony\DevApi\Model\Authentication\OauthClient;

/**
 * Class CreatePost
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreatePost implements CreatePostInterface
{
    /**
     * @var TokenBased
     */
    protected $tokenBased;

    protected $oauthClient;

    /**
     * CreatePost Constructor.
     *
     * @param TokenBased $tokenBased
     * @param OauthClient $oauthClient
     */
    public function __construct(
        TokenBased $tokenBased,
        OauthClient $oauthClient
    ) {
        $this->tokenBased = $tokenBased;
        $this->oauthClient = $oauthClient;
    }

    /**
     * @api
     * @param string[] $orderData
     * @return array|mixed|string
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function init(array $orderData)
    {

        $this->oauthClient->sign();
        exit;

        $token = $this->tokenBased->getCustomerAccessToken();

        //$ch = curl_init("https://magentoce232pwa.local/index.php/rest/V1/customers/4");
        $ch = curl_init("https://magentoce232pwa.local/index.php/rest/V1/customers/me");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));

        $result = curl_exec($ch);


        var_dump(json_decode($result, true));

        return $orderData;
    }
}
