<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\DevApi\Model;

use Magento\Framework\App\Action\Context;
use Rony\DevApi\Api\DemoInterface;
use Rony\DevApi\Logger\Logger;

/**
 * Class Demo
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Demo implements DemoInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Context $context
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        Logger $logger
    ) {
        $this->_request = $context->getRequest();
        $this->logger = $logger;
    }

    /**
     * Send request
     *
     * @api
     * @return mixed|string
     */
    public function init()
    {
        //https://magentoce232pwa.local/checkout/onepage/success/?data=ZXA1VHZJYS9qN3dhOXEydkVacmFSWVNzNHZQb1JLRGhaVFR3ZFJXdW1ybER0QjQrOWo2MzloaysvRXM3MFB5dnByQk8yL05JR2p2eGk5SGlSZU0xSDJoMkUvNWRMbHFiRW9pZ0J6ckswdm5iNHN5OWFnMWRzUXlVTUl6T09GYjNSZWN5VFZ6eDFadzNnRW8yRDBHYndPTVV1bXZJSmNnYUFHZkhRK0Fqb1p3PQ==&checksum=b296684088a08dd01e8fa2905566acf5
        $this->logger->critical('Alepay response: ', array($this->_request->getParam('data'), $this->_request->getParam('checksum')));
        return "Hello, " . $this->_request->getParam('data') . ' ' . $this->_request->getParam('checksum');
        //return __LINE__ . __FILE__;
    }
}
