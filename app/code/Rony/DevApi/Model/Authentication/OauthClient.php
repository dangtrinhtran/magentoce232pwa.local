<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\DevApi\Model\Authentication;

use OAuth\Common\Consumer\Credentials;
use OAuth\OAuth1\Signature\Signature;
use OAuth\Common\Http\Uri\Uri;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Http\Client;
use Rony\DevApi\Model\Authentication\OAuthBased;

/**
 * Class OauthClient
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OauthClient
{
    protected $token        = null;

    public function sign()
    {
        /*
        $oauthConsumerKey = 'miuhyqzf3xyv9zk5hvf7dywlmn1rsqbr';
        $oauthConsumerSecret = 'clsj3e4rjo09dmmtpfknx3f89kjg2qo8';
        $magentoBaseUrl = 'https://magentoce232pwa.local/';
        $oauthAccessTokenSecret = 'x1ehedbsd6vw00x849pmtmxfx95c4hvd';
        $oauthSignatureMethod = 'HMAC-SHA1';
        //Basic information needed for requests
        $restResourceUri = 'https://magentoce232pwa.local/index.php/rest/V1/cmsBlock/1';
        $oauthTimestamp = time();
        $oauthAccessToken = 'iflcy3y4h8ivig79efxbpiarof2iaku4';
        $oauthNonce = md5(openssl_random_pseudo_bytes(20));

        //Create oauth signature
        $params = [
            'oauth_consumer_key' => $oauthConsumerKey,
            'oauth_nonce' => $oauthNonce,
            'oauth_signature_method' => $oauthSignatureMethod,
            'oauth_timestamp' => $oauthTimestamp,
            'oauth_token' => $oauthAccessToken,
            'oauth_version' => '1.0',
        ];

        $credentials = new Credentials($oauthConsumerKey, $oauthConsumerSecret, $magentoBaseUrl);
        $signature = new Signature($credentials);
        $signature->setTokenSecret($oauthAccessTokenSecret);
        $signature->setHashingAlgorithm($oauthSignatureMethod);

        $oauthUri = new Uri($restResourceUri);
        $oauthSignature = $signature->getSignature($oauthUri, $params, 'GET');

        $params['oauth_signature'] = $oauthSignature;

        //Create oauth Authorization header
        $oauthHeader = 'OAuth ';
        foreach ($params as $key => $value) {
            $oauthHeader .= sprintf('%s="%s", ', $key, $value);
        }
        $oauthHeader = rtrim($oauthHeader, ' ,');


        $httpHeaders = new Headers();
        $httpHeaders->addHeaders([
            'Authorization' => $oauthHeader,
            'Content-Type' => 'application/json'
        ]);


        // create request for cms block using oauth Authorization header
        $request = new Request();
        $request->setHeaders($httpHeaders);
        $request->setUri($restResourceUri);
        $request->setMethod(Request::METHOD_GET);


        // create client and get cms block response
        $client = new Client();
        $options = [
            'adapter'   => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 0,
            'timeout' => 30
        ];
        $client->setOptions($options);

        $response = $client->send($request);

        var_dump(json_decode($response->getBody(), true));

        */

        /*
        $oauthConsumerKey = 'miuhyqzf3xyv9zk5hvf7dywlmn1rsqbr';
        $oauthConsumerSecret = 'clsj3e4rjo09dmmtpfknx3f89kjg2qo8';
        $magentoBaseUrl = 'https://magentoce232pwa.local/';
        $oauthAccessTokenSecret = 'x1ehedbsd6vw00x849pmtmxfx95c4hvd';
        $oauthSignatureMethod = 'HMAC-SHA1';
        //Basic information needed for requests
        $restResourceUri = 'https://magentoce232pwa.local/index.php/rest/V1/cmsBlock/1';
        $oauthTimestamp = time();
        $oauthAccessToken = 'iflcy3y4h8ivig79efxbpiarof2iaku4';
        $oauthNonce = md5(openssl_random_pseudo_bytes(20));

        //Create oauth signature
        $params = [
            'oauth_consumer_key' => $oauthConsumerKey,
            'oauth_nonce' => $oauthNonce,
            'oauth_signature_method' => $oauthSignatureMethod,
            'oauth_timestamp' => $oauthTimestamp,
            'oauth_token' => $oauthAccessToken,
            'oauth_version' => '1.0',
        ];

        $credentials = new Credentials($oauthConsumerKey, $oauthConsumerSecret, $magentoBaseUrl);
        $signature = new Signature($credentials);
        $signature->setTokenSecret($oauthAccessTokenSecret);
        $signature->setHashingAlgorithm($oauthSignatureMethod);

        $oauthUri = new Uri($restResourceUri);
        $oauthSignature = $signature->getSignature($oauthUri, $params, 'GET');

        $params['oauth_signature'] = $oauthSignature;

        //Create oauth Authorization header
        $oauthHeader = 'OAuth ';
        foreach ($params as $key => $value) {
            $oauthHeader .= sprintf('%s="%s", ', $key, $value);
        }
        $oauthHeader = rtrim($oauthHeader, ' ,');


        $headers = array(
            "Authorization" =>  $oauthHeader,
            "Content-Type" =>  "Content-Type: application/x-www-form-urlencoded",
            "Host" =>  "Host: https://magentoce232pwa.local",
            "Connection" =>  "Connection: close",
            "Content-length" =>  "Content-length: 0",
        );
        $context = stream_context_create(
            array(
                'http' => array(
                    'method'           => 'GET',
                    'header'           => implode("\r\n", array_values($headers)),
                    'content'          => "",
                    'protocol_version' => '1.1',
                    'user_agent'       => "PHPoAuthLib",
                    'max_redirects'    => 5,
                    'timeout'          => 18000
                ),
            )
        );
        $response = file_get_contents('https://magentoce232pwa.local/oauth/token/request', false, $context);
        var_dump($response);
        */

/*
        $consumerKey = 'miuhyqzf3xyv9zk5hvf7dywlmn1rsqbr';
        $consumerSecret = 'clsj3e4rjo09dmmtpfknx3f89kjg2qo8';
        $magentoBaseUrl = 'https://magentoce232pwa.local/';
        $oauthVerifier = null;


        $credentials = new \OAuth\Common\Consumer\Credentials($consumerKey, $consumerSecret, $magentoBaseUrl);
        $oAuthClient = new OAuthBased($credentials);
        $requestToken = $oAuthClient->requestRequestToken();

        $accessToken = $oAuthClient->requestAccessToken(
            $requestToken->getRequestToken(),
            $oauthVerifier,
            $requestToken->getRequestTokenSecret()
        );

        var_dump($accessToken);
*/

        $dateTime = new \DateTime();

        $params = [
            'oauth_consumer_key' => 'miuhyqzf3xyv9zk5hvf7dywlmn1rsqbr',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => md5(openssl_random_pseudo_bytes(20)),
            'oauth_timestamp' => $dateTime->format('U'),
            'oauth_version' => '1.0',
        ];


        $credentials = new Credentials('miuhyqzf3xyv9zk5hvf7dywlmn1rsqbr', 'clsj3e4rjo09dmmtpfknx3f89kjg2qo8', 'https://magentoce232pwa.local/');
        $signature = new Signature($credentials);
        $signature->setTokenSecret('c97m807luhr0dsn0pxo4cqffhyvvnjwq');
        $signature->setHashingAlgorithm('HMAC-SHA1');

        $oauthUri = new Uri('https://magentoce232pwa.local/index.php/rest/V1/cmsBlock/1');
        $oauthSignature = $signature->getSignature($oauthUri, $params, 'POST');

        $params['oauth_signature'] = $oauthSignature;

        //Create oauth Authorization header
        $authorizationHeader = 'OAuth ';
        $delimiter = '';

        foreach ($params as $key => $value) {
            $authorizationHeader .= $delimiter . rawurlencode($key) . '="' . rawurlencode($value) . '"';
            $delimiter = ', ';
        }

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => $authorizationHeader,
            ],
        ]);


        $response = $client->post('https://magentoce232pwa.local/oauth/token/request');

        var_dump(json_decode($response->getBody()->getContents()));


    }


}
