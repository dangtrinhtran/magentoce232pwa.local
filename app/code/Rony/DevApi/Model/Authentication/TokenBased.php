<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\DevApi\Model\Authentication;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\UriInterface;

/**
 * Class TokenBased
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TokenBased
{
    const API_USERNAME       = 'devapi';

    const API_PASSWORD       = 'Kimdanh8xx';

    const API_ADMIN_TOKEN    = 'https://magentoce232pwa.local/index.php/rest/V1/integration/admin/token';

    const API_CUSTOMER_TOKEN = 'https://magentoce232pwa.local/index.php/rest/V1/integration/customer/token';

    const CUSTOMER_USERNAME  = 'apidev@dev.com';

    const CUSTOMER_PASSWORD  = 'Kimdanh8xx';

    protected $adminToken    = null;

    protected $customerToken = null;

    /**
     * @return mixed|null
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function getCustomerAccessToken()
    {
        if (empty($this->customerToken)) {
            $this->customerToken = $this->fetchAccessToken(self::API_CUSTOMER_TOKEN, self::CUSTOMER_USERNAME, self::CUSTOMER_PASSWORD);
        }

        return $this->customerToken;
    }

    /**
     * @return mixed|null
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function getAdminAccessToken()
    {
        if (empty($this->adminToken)) {
            $this->adminToken = $this->fetchAccessToken(self::API_ADMIN_TOKEN, self::API_USERNAME, self::API_PASSWORD);
        }

        return $this->adminToken;
    }

    /**
     * @param string|UriInterface $baseUri
     * @param string $username
     * @param string $password
     * @return mixed|null
     * @throws \Magento\Framework\Webapi\Exception
     */
    protected function fetchAccessToken($baseUri, $username, $password)
    {
        $client = new Client([
            'headers' => [
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'Cache-Control' => 'no-cache',
            ],
        ]);
        try {
            $response = $client->post($baseUri, [
                'body' => json_encode([
                    'username' => $username,
                    'password' => $password,
                ], JSON_UNESCAPED_UNICODE)
            ]);

            if ($response) {
                return json_decode($response->getBody()->getContents());
            }

            return null;
        } catch (ClientException $e) {
            throw new \Magento\Framework\Webapi\Exception(
                __('Service unavailable: %1', $e->getMessage())
            );
        }
    }

}
