<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Rony\DevApi\Api;

/**
 * Class DemoInterface
 *
 * @api
 * @since 100.0.1
 */
interface DemoInterface
{
    /**
     * Send request
     *
     * @api
     * @return mixed
     * @since 100.0.1
     */
    public function init();
}
