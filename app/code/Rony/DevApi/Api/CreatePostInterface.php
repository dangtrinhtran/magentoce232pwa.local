<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Rony\DevApi\Api;

/**
 * Class DemoInterface
 *
 * @api
 * @since 100.0.1
 */
interface CreatePostInterface
{
    /**
     * @api
     * @param string[] $orderData
     * @return mixed|string
     */
    public function init(array $orderData);
}
