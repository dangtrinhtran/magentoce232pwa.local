<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\DevPlugins\Plugin\Checkout\Model;

use Magento\Catalog\Model\Product;

/**
 * Plugin - Before methods
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CartUpdater
{

    /**
     * Before addProduct plugin
     *
     * @param \Magento\Checkout\Model\Cart $subject
     * @param int|Product $productInfo
     * @param \Magento\Framework\DataObject|int|array $requestInfo
     * @return array
     */
    public function beforeAddProduct(\Magento\Checkout\Model\Cart $subject, $productInfo, $requestInfo = null): array
    {
        //increasing quantity to 5
        $requestInfo['qty'] = 5;

        return array($productInfo, $requestInfo);
    }

}
