<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\VnpayPayment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Rony\VnpayPayment\Logger\Logger as LoggerVnpay;

/**
 * Class Vnpay
 * @package Rony\VnpayPayment\Helper
 */
class Vnpay extends AbstractHelper
{
    const DEFAULT_ORDER_STATUS  = 'pending';

    const VNPAY_TMN_CODE        = 'QFBO5ZFS';

    const VNPAY_HASH_SECRET     = 'XCYTGSNPIUROMDMOUCTCHZQEHNDUKFGF';

    const VNPAY_HTTP_REQUEST_URL_TESTING = 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html';


    /**
     * @var LoggerVnpay
     */
    protected $logger;

    /**
     * Vnpay Constructor.
     *
     * @param Context $context
     * @param LoggerVnpay $logger
     */
    public function __construct(Context $context, LoggerVnpay $logger)
    {
        $this->logger   = $logger;
        parent::__construct($context);
    }


}
