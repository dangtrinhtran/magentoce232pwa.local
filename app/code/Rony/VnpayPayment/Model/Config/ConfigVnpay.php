<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\VnpayPayment\Model\Config;

use Magento\Payment\Gateway\Config\Config;

/**
 * Class ConfigVnpay
 * @package Rony\VnpayPayment\Model\Config
 */
class ConfigVnpay extends Config
{}
