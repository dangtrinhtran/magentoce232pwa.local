<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\VnpayPayment\Model;

use Magento\Checkout\Model\Session;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Api\OrderPaymentRepositoryInterface;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Rony\VnpayPayment\Helper\Vnpay as HelperVnpay;
use Rony\VnpayPayment\Helper\Data as HelperData;
use Rony\VnpayPayment\Api\VnpayInterface;
use Rony\VnpayPayment\Logger\Logger as LoggerVnpay;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class VnpayService
 * @package Rony\VnpayPayment\Model
 */
class VnpayService implements VnpayInterface
{
    /**
     * @var LoggerVnpay
     */
    protected $logger;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var HelperVnpay
     */
    private $helperVnpay;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var OrderStatusHistoryRepositoryInterface
     */
    private $orderStatusHistoryRepository;

    /**
     * @var HistoryFactory
     */
    private $orderHistoryFactory;

    /**
     * @var BuilderInterface
     */
    private $transactionBuilder;

    /**
     * @var OrderPaymentRepositoryInterface
     */
    private $orderPaymentRepository;

    /**
     * @var TransactionRepositoryInterface
     */
    private $transactionRepository;

    /**
     * VnpayService Constructor.
     *
     * @param Context $context
     * @param LoggerVnpay $logger
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param HelperVnpay $helperVnpay
     * @param HelperData $helperData
     * @param Session $checkoutSession
     * @param OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository
     * @param HistoryFactory $orderHistoryFactory
     * @param BuilderInterface $transactionBuilder
     * @param OrderPaymentRepositoryInterface $orderPaymentRepository
     * @param TransactionRepositoryInterface $transactionRepository
     */
    public function __construct(
        Context $context,
        LoggerVnpay $logger,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        HelperVnpay $helperVnpay,
        HelperData $helperData,
        Session $checkoutSession,
        OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository,
        HistoryFactory $orderHistoryFactory,
        BuilderInterface $transactionBuilder,
        OrderPaymentRepositoryInterface $orderPaymentRepository,
        TransactionRepositoryInterface $transactionRepository
    ) {
        $this->request          = $context->getRequest();
        $this->logger           = $logger;
        $this->orderRepository  = $orderRepository;
        $this->messageManager   = $context->getMessageManager();
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->helperVnpay      = $helperVnpay;
        $this->helperData       = $helperData;
        $this->checkoutSession  = $checkoutSession;
        $this->orderStatusHistoryRepository = $orderStatusHistoryRepository;
        $this->orderHistoryFactory          = $orderHistoryFactory;
        $this->transactionBuilder           = $transactionBuilder;
        $this->orderPaymentRepository       = $orderPaymentRepository;
        $this->transactionRepository        = $transactionRepository;
    }

    /**
     * Send request to Alepay
     *
     * @api
     * @param int|string $orderId
     * @return mixed|string|bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function init($orderId)
    {
        $response = [];
        if ($orderId) {
            try {
                $order = $this->orderRepository->get($orderId);
                if ($order instanceof \Magento\Sales\Model\Order && HelperVnpay::DEFAULT_ORDER_STATUS === $order->getStatus()) {
                    try {
                        $orderCode = $order->getIncrementId();

                        $dataRequest = [
                            'vnp_Version'           => '2.0.0',
                            'vnp_Command'           => 'pay',
                            'vnp_TmnCode'           => HelperVnpay::VNPAY_TMN_CODE,
                            'vnp_Amount'            => '1900000',
                            //'vnp_BankCode'          => 'VNBANK',
                            'vnp_CreateDate'        => date('YmdHis'),
                            'vnp_CurrCode'          => 'VND',
                            'vnp_IpAddr'            => $_SERVER['REMOTE_ADDR'],
                            'vnp_Locale'            => 'vn',//en
                            'vnp_OrderInfo'         => 'Description of Order',
                            'vnp_OrderType'         => '200000',
                            'vnp_ReturnUrl'         => $this->helperData->getUrl('vnpay/payment/callback'),
                            'vnp_TxnRef'            => $orderCode,
                            //'vnp_SecureHashType'    => '',
                            //'vnp_SecureHash'        => '',
                        ];

                        ksort($dataRequest);
                        $query = '';
                        $i = 0;
                        $hashData = '';
                        foreach ($dataRequest as $key => $value) {
                            if (1 === $i) {
                                $hashData .= '&' . $key . '=' . $value;
                            } else {
                                $hashData .= $key . '=' . $value;
                                $i = 1;
                            }
                            $query .= urlencode($key) . '=' . urlencode($value) . '&';
                        }

                        $httpRequestUrl = HelperVnpay::VNPAY_HTTP_REQUEST_URL_TESTING . '?' . $query;

                        $vnpSecureHash = hash('sha256',HelperVnpay::VNPAY_HASH_SECRET . $hashData);
                        $httpRequestUrl .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;

                        //Log
                        $this->logger->critical('Data request to VNPay: ', [$orderId, $dataRequest, $httpRequestUrl]);

                        //Add order status
                        $order->addStatusToHistory(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, __('Payment pending by customer.'));
                        $this->orderRepository->save($order);

                        $response = [
                            'success'       => true,
                            'message'       => null,
                            'checkoutUrl'   => $httpRequestUrl,
                        ];

                        return json_encode($response);
                    } catch (\Exception $e) {
                        $this->logger->critical($e);
                    }
                }
            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                $this->logger->critical($exception);
            }
        }
        $this->messageManager->addErrorMessage(__('Service unavailable!'));
        $response = [
            'success'       => false,
            'message'       => __('Service unavailable!'),
            'checkoutUrl'   => $this->helperData->getUrl('checkout/cart'),
        ];

        return json_encode($response);
    }

    /**
     * @api
     * @return mixed
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function callback()
    {}

    /**
     * @api
     * @return mixed
     */
    public function ipn()
    {
        // TODO: Implement ipn() method.
        try {
            $params = $this->request->getParams();
            //Log
            $this->logger->critical('Response from VNPay: ', $params);

            $inputData = array();
            $returnData = array();

            foreach ($params as $key => $value) {
                if (substr($key, 0, 4) == "vnp_") {
                    $inputData[$key] = $value;
                }
            }

            $vnpSecureHash = $inputData['vnp_SecureHash'];
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $i = 0;
            $hashData = '';

            foreach ($inputData as $key => $value) {
                if (1 === $i) {
                    $hashData = $hashData . '&' . $key . '=' . $value;
                } else {
                    $hashData = $hashData . $key . '=' . $value;
                    $i = 1;
                }
            }

            $secureHash = hash('sha256',HelperVnpay::VNPAY_HASH_SECRET . $hashData);

            $orderId = $this->checkoutSession->getLastOrderId();
            if (empty($orderId)) {
                $incrementId = $inputData['vnp_TxnRef'];
                $order = $this->getOrderByIncrementId($incrementId);
            } else {
                $order = $this->orderRepository->get($orderId);
            }
            if ($vnpSecureHash === $secureHash) {
                if ($order instanceof \Magento\Sales\Model\Order) {
                    $orderStatus = $order->getStatus();
                    $extOrderId = (empty($orderId)) ? $order->getId() : $orderId;
                    if ($orderStatus === \Magento\Sales\Model\Order::STATE_PROCESSING || $orderStatus === \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {
                        if ('00' === $inputData['vnp_ResponseCode']) {
                            $order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
                            $this->createTransaction($order, $inputData);
                            $order->setExtOrderId($extOrderId);
                            $this->addOrderHistory($order, __('VNPay 2.0.0 Transaction Has Been Successful.'));
                        } else {
                            $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                            $this->createTransaction($order, $inputData);
                            $order->setExtOrderId($extOrderId);
                            $this->addOrderHistory($order, __('VNPay 2.0.0 Transaction Failed.'));
                        }
                        $this->orderRepository->save($order);
                        $this->checkoutSession->setClearQuote(true);
                        //TODO
                        $returnData['RspCode'] = '00';
                        $returnData['Message'] = 'Confirm Success!';
                    } else {
                        $returnData['RspCode'] = '02';
                        $returnData['Message'] = 'Order already confirmed!';
                    }
                } else {
                    $returnData['RspCode'] = '01';
                    $returnData['Message'] = 'Order not found!';
                }
            }
            $returnData['RspCode'] = '97';
            $returnData['Message'] = 'Invalid checksum!';
        } catch (\Exception $e) {
            $returnData['RspCode'] = '99';
            $returnData['Message'] = 'Unknown error!';
        }

        return json_encode($returnData);

    }

    /**
     * @param string $incrementId
     * @return bool|\Magento\Sales\Api\Data\OrderInterface
     */
    private function getOrderByIncrementId(string $incrementId)
    {
        $this->searchCriteriaBuilder->addFilter('increment_id', $incrementId);
        $order = $this->orderRepository->getList(
            $this->searchCriteriaBuilder->create()
        );
        $items = $order->getItems();
        if (!empty($items)) {
            foreach ($items as $item) {
                return $item;
            }
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param string $comment
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    private function addOrderHistory($order, $comment)
    {
        $history = $this->orderHistoryFactory->create()
            ->setComment($comment)
            ->setEntityName('order')
            ->setOrder($order);
        if (\Magento\Sales\Model\Order::STATE_COMPLETE === $order->getStatus()) {
            $history->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
        } else {
            $history->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
        }
        try {
            $this->orderRepository->save($order);
            $this->orderStatusHistoryRepository->save($history);

            return true;
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order|null $order
     * @param array $paymentData
     * @return bool
     */
    private function createTransaction($order = null, $paymentData = array())
    {
        try {
            //Get payment from order
            /** @var \Magento\Sales\Model\Order\Payment $payment */
            $payment = $order->getPayment();
            $payment->setLastTransId($paymentData['vnp_TransactionNo']);
            $payment->setTransactionId($paymentData['vnp_TransactionNo']);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
            );
            $formattedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );

            $message = __('The authorized amount is %1.', $formattedPrice);
            //Get the object of builder class
            $trans = $this->transactionBuilder;
            /** @var \Magento\Sales\Model\Order\Payment\Transaction $transaction */
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentData['vnp_TransactionNo'])
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                //Build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setParentTransactionId(null);
            $this->orderPaymentRepository->save($payment);
            $this->orderRepository->save($order);

            return  $this->transactionRepository->save($transaction)->getTransactionId();
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return false;
    }

}
