<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\VnpayPayment\Model;

use Magento\Payment\Model\Method\Adapter;

/**
 * Class Vnpay
 * @package Rony\VnpayPayment\Model
 */
class Vnpay extends Adapter
{
    /**
     * VNPay Payment Code
     */
    const PAYMENT_METHOD_VNPAY_CODE = 'vnpay';
}
