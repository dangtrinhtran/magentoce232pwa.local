<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\VnpayPayment\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Sales\Api\OrderRepositoryInterface;
use Rony\VnpayPayment\Helper\Vnpay as HelperVnpay;
use Rony\VnpayPayment\Helper\Data as HelperData;
use Rony\VnpayPayment\Logger\Logger as LoggerVnpay;

/**
 * Class Callback
 * @package Rony\VnpayPayment\Controller\Payment
 */
class Callback extends Action
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var HelperVnpay
     */
    private $helperVnpay;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var LoggerVnpay
     */
    protected $logger;

    /**
     * Cancel Constructor.
     *
     * @param Context $context
     * @param Session $checkoutSession
     * @param OrderRepositoryInterface $orderRepository
     * @param HelperVnpay $helperVnpay
     * @param HelperData $helperData
     * @param LoggerVnpay $logger
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderRepositoryInterface $orderRepository,
        HelperVnpay $helperVnpay,
        HelperData $helperData,
        LoggerVnpay $logger
    ) {
        parent::__construct($context);
        $this->checkoutSession  = $checkoutSession;
        $this->orderRepository  = $orderRepository;
        $this->helperVnpay      = $helperVnpay;
        $this->helperData       = $helperData;
        $this->logger           = $logger;
    }

    /**
     * Callback VNPay Checkout
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try {
            // TODO verify if this logic of order cancellation is deprecated
            $params        = $this->_request->getParams();
            $vnpSecureHashResponse = $params['vnp_SecureHash'];
            $inputData = [];
            foreach ($params as $key => $value) {
                $inputData[$key] = $value;
            }
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);

            $i = 0;
            $hashData = '';
            foreach ($inputData as $key => $value) {
                if (1 === $i) {
                    $hashData = $hashData . '&' . $key . '=' . $value;
                } else {
                    $hashData = $hashData . $key . '=' . $value;
                    $i = 1;
                }
            }
            $secureHash = hash('sha256',HelperVnpay::VNPAY_HASH_SECRET . $hashData);
            //Log
            $this->logger->critical('Callback from VNPay: ', $params);

            if ($vnpSecureHashResponse === $secureHash) {
                if ('00' === $params['vnp_ResponseCode']) {
                    $this->messageManager->addSuccessMessage(
                        __('VNPay Checkout and Order have been processed.')
                    );
                    $path = 'checkout/onepage/success';
                } else {
                    $this->messageManager->addSuccessMessage(
                        __('Something went wrong while processing your order.')
                    );
                    $path = 'checkout/onepage/failure';
                }

                return $resultRedirect->setPath($path, ['_secure' => true]);
            }
            $this->messageManager->addErrorMessage(__('Unable to process VNPay Checkout'));

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Unable to process VNPay Checkout'));
        }

        return $resultRedirect->setPath('checkout/cart', ['_secure' => true]);
    }
}
