<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Rony\VnpayPayment\Api;

/**
 * interface VnpayInterface
 *
 * @api
 * @since 100.0.1
 */
interface VnpayInterface
{
    /**
     * Send request to VNPay
     *
     * @api
     * @param string|int $orderId
     * @return mixed
     * @since 100.0.1
     */
    public function init($orderId);

    /**
     * Get response from VNPay
     *
     * @api
     * @return mixed
     * @since 100.0.1
     */
    public function callback();

    /**
     * VNPay will call to this URL for update order and response to VNPay.
     *
     * @api
     * @return mixed
     * @since 100.0.1
     */
    public function ipn();
}
