/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* @api */
define([
    'ko',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/action/redirect-on-success',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'mage/url',
    'mage/storage',
    'jquery'
], function (
    ko,
    Component,
    additionalValidators,
    redirectOnSuccessAction,
    checkoutData,
    checkoutDataResolver,
    urlBuilder,
    storage,
    $
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Rony_AlepayPayment/payment/alepay'
        },

        /**
         * Place order.
         */
        placeOrder: function (data, event) {
            let self = this;

            if (event) {
                event.preventDefault();
            }

            if (this.validate() && additionalValidators.validate()) {
                this.isPlaceOrderActionAllowed(false);

                this.getPlaceOrderDeferredObject()
                    .fail(
                        function () {
                            self.isPlaceOrderActionAllowed(true);
                        }
                    ).done(
                    function (orderID) {
                        var initAlepayApi = urlBuilder.build('rest/V1/alepay/init');
                        var dataRequest = {
                            "orderId": orderID
                        };
                        //console.log(dataRequest);
                        //console.log(JSON.stringify(dataRequest));
                        storage.post(
                            initAlepayApi,
                            JSON.stringify(dataRequest),
                            true
                        ).done(
                            function (response) {
                                var data = JSON.parse(response);
                                //console.log(data);
                                window.location.replace(data.checkoutUrl);
                                self.redirectAfterPlaceOrder = false;
                                /*if (data.success) {
                                    window.location.replace(data.checkoutUrl);
                                    self.redirectAfterPlaceOrder = false;
                                } else {
                                    redirectOnSuccessAction.execute();
                                }*/
                            }
                        ).fail(
                            function (response) {
                                console.log(response);
                                self.isPlaceOrderActionAllowed(true);
                            }
                        );

                        self.afterPlaceOrder();

                        /*
                        if (self.redirectAfterPlaceOrder) {
                            redirectOnSuccessAction.execute();
                        }*/
                    }
                );
                //return false;
                return true;
            }

            return false;
        },

        /**
         * Get payment logo
         * @returns {String}
         */
        getLogo: function () {
            return window.checkoutConfig.payment.alepay.logoUrl;
        },

        /**
         * Returns send check to info.
         *
         * @return {*}
         */
        getMailingAddress: function () {
            return window.checkoutConfig.payment.checkmo.mailingAddress;
        }
    });
});
