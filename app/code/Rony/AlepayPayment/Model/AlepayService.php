<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Model;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Context;
use Rony\AlepayPayment\Api\AlepayInterface;
use Rony\AlepayPayment\Gateway\Config\Config;
use Rony\AlepayPayment\Logger\Logger as LoggerAlepay;
use Magento\Sales\Api\OrderRepositoryInterface;
use Rony\AlepayPayment\Helper\Alepay as HelperAlepay;
use Rony\AlepayPayment\Helper\Data as HelperData;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class AlepayService
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AlepayService implements AlepayInterface
{
    /**
     * @var LoggerAlepay
     */
    protected $logger;

    /**
     * @var HelperAlepay
     */
    protected $helperAlepay;

    /**
     * @var HelperData
     */
    protected $helperData;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Config
     */
    private $config;

    /**
     * AlepayService Constructor.
     *
     * @param Context $context
     * @param LoggerAlepay $logger
     * @param OrderRepositoryInterface $orderRepository
     * @param HelperAlepay $helperAlepay,
     * @param HelperData $helperData
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Config $config
     */
    public function __construct(
        Context $context,
        LoggerAlepay $logger,
        OrderRepositoryInterface $orderRepository,
        HelperAlepay $helperAlepay,
        HelperData $helperData,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Config $config
    ) {
        $this->request          = $context->getRequest();
        $this->logger           = $logger;
        $this->orderRepository  = $orderRepository;
        $this->helperAlepay     = $helperAlepay;
        $this->helperData       = $helperData;
        $this->messageManager   = $context->getMessageManager();
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->config           = $config;
    }

    /**
     * Send request to Alepay
     *
     * @api
     * @param int|string $orderId
     * @return mixed|string|bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function init($orderId)
    {
        $response = [];
        if ($orderId) {
            try {
                $order = $this->orderRepository->get($orderId);
                if ($order instanceof \Magento\Sales\Model\Order && HelperAlepay::DEFAULT_ORDER_STATUS === $order->getStatus()) {
                    $dataRequest        = [];
                    $orderCode          = $order->getIncrementId();
                    $amount             = $order->getGrandTotal();
                    $currency           = $order->getOrderCurrencyCode();
                    $orderDescription   = 'Description of Order';
                    $totalItem          = $order->getTotalQtyOrdered();
                    $checkoutType       = 3;
                    $returnUrl          = $this->helperData->getUrl('alepay/payment/callback');
                    $cancelUrl          = $this->helperData->getUrl('alepay/payment/cancel');
                    $billingAddress     = $order->getBillingAddress();
                    $buyerName          = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
                    $buyerEmail         = trim($billingAddress->getEmail());
                    $buyerPhone         = trim($billingAddress->getTelephone());
                    $buyerAddress       = $this->helperData->getStreetToString($billingAddress->getStreet());
                    $buyerCity          = $billingAddress->getCity();
                    $buyerCountry       = $this->helperData->getCountryNameByCode($billingAddress->getCountryId());
                    $paymentHours       = 48;

                    $dataRequest['orderCode']           = $orderCode;
                    $dataRequest['amount']              = '4999000';//$amount;
                    $dataRequest['currency']            = 'VND';//$currency;
                    $dataRequest['orderDescription']    = $orderDescription;
                    $dataRequest['totalItem']           = (int)$totalItem;
                    $dataRequest['checkoutType']        = $checkoutType;
                    $dataRequest['returnUrl']           = $returnUrl;
                    $dataRequest['cancelUrl']           = $cancelUrl;
                    $dataRequest['buyerName']           = $buyerName;
                    $dataRequest['buyerEmail']          = $buyerEmail;
                    $dataRequest['buyerPhone']          = $buyerPhone;
                    $dataRequest['buyerAddress']        = $buyerAddress;
                    $dataRequest['buyerCity']           = $buyerCity;
                    $dataRequest['buyerCountry']        = $buyerCountry;
                    $dataRequest['paymentHours']        = $paymentHours;
                    $dataRequest['month']               = 3;
                    $dataRequest['allowDomestic']       = true;

                    try {
                        $result = $this->helperAlepay->processAlepayConnection($dataRequest);
                        //Log
                        $this->logger->critical('Data request to Alepay: ', $dataRequest);
                        if ($result && $result->token) {
                            //Add order status
                            $order->addStatusToHistory(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, __('Payment pending by customer.'));
                            $this->orderRepository->save($order);

                            $response = [
                                'success'       => true,
                                'message'       => null,
                                'checkoutUrl'   => $result->checkoutUrl,
                            ];

                            return json_encode($response);
                        }
                    } catch (\Exception $e) {
                        $this->logger->critical($e);
                    }
                }
            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                $this->logger->critical($exception);
            }
        }
        $this->messageManager->addErrorMessage(__('Service unavailable!'));
        $response = [
            'success'       => false,
            'message'       => __('Service unavailable!'),
            'checkoutUrl'   => $this->helperData->getUrl('checkout/cart'),
        ];

        return json_encode($response);
    }

    /**
     * @api
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function callback()
    {
        //$order =  $this->getOrderByIncrementId('000000040');
        //var_dump($order instanceof \Magento\Sales\Model\Order);
        /*var_dump($this->config->getEnvironment($this->helperData->getStoreId()));
        var_dump($this->config->getTokenKey());
        var_dump($this->config->getChecksumKey());
        var_dump($this->config->getEncryptKey());
        var_dump($this->config->getRequestUrl('checkout/v1/request-order', $this->helperData->getStoreId()));
        var_dump($this->config->getRequestUrl('checkout/v1/get-transaction-info', $this->helperData->getStoreId()));
        exit;*/
        // TODO: Implement callback() method.
        $request            = $this->request;
        $dataResponse       = $request->getParam('data');
        $checksumResponse   = $request->getParam('checksum');
        $error = __('Invalid response.');

        $verify = $this->helperAlepay->verifyResponseDataFromAlepay(base64_decode($dataResponse), $checksumResponse);

        if ($dataResponse && $checksumResponse && $verify) {
            $data = $this->helperAlepay->decryptCallbackData($dataResponse, $this->config->getEncryptKey());
            $dataCallback = json_decode($data);
            $transactionInfo = $this->helperAlepay->getTransactionInfoFromAlepay($dataCallback->data);
            //Log
            $this->logger->critical('TransactionInfo Decrypted: ', [$transactionInfo]);
            if ($transactionInfo) {
                try {


                    return json_decode($transactionInfo, true);
                } catch (\Exception $e) {
                    $this->logger->critical($e->getMessage());
                    $error = __('Invalid TransactionInfo.');
                    throw new \Magento\Framework\Webapi\Exception(
                        __('Service unavailable: %1', $error)
                    );
                }
            }
        } else {
            throw new \Magento\Framework\Webapi\Exception(
                __('Service unavailable: %1', $error)
            );
        }
    }

    /**
     * @param string $incrementId
     * @return bool|\Magento\Sales\Api\Data\OrderInterface
     */
    private function getOrderByIncrementId(string $incrementId)
    {
        $this->searchCriteriaBuilder->addFilter('increment_id', $incrementId);
        $order = $this->orderRepository->getList(
            $this->searchCriteriaBuilder->create()
        );
        $items = $order->getItems();
        if (!empty($items)) {
            foreach ($items as $item) {
                return $item;
            }
        }

        return false;
    }

}
