<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Rony\AlepayPayment\Model\Config;

/**
 * Class AlepayConfigProvider
 * @package Rony\AlepayPayment\Model
 * @api
 * @since 100.0.1
 */
class AlepayConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * AlepayConfigProvider Constructor.
     *
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                'alepay' => [
                    'logoUrl' => $this->config->getViewFileUrl('Rony_AlepayPayment::images/alepay_logo.png'),
                ]
            ]
        ];
    }
}
