<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Model;

use Magento\Payment\Model\Method\Adapter;

/**
 * Class Alepay
 * @package Rony\AlepayPayment\Model
 */
class Alepay extends Adapter
{
    /**
     * Alepay Payment Code
     */
    const PAYMENT_METHOD_ALEPAY_CODE = 'alepay';
}
