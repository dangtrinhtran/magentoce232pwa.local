<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Rony\AlepayPayment\Api;

/**
 * interface AlepayInterface
 *
 * @api
 * @since 100.0.1
 */
interface AlepayInterface
{
    /**
     * Send request to Alepay
     *
     * @api
     * @param string|int $orderId
     * @return mixed
     * @since 100.0.1
     */
    public function init($orderId);

    /**
     * Get response from Alepay
     *
     * @api
     * @return mixed
     * @since 100.0.1
     */
    public function callback();
}
