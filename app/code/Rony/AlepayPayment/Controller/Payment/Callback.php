<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Api\OrderPaymentRepositoryInterface;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Rony\AlepayPayment\Helper\Alepay as HelperAlepay;
use Rony\AlepayPayment\Logger\Logger as LoggerAlepay;
use Rony\AlepayPayment\Helper\Data as HelperData;
use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Session;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Rony\AlepayPayment\Gateway\Config\Config;

/**
 * Class Callback
 * @package Rony\AlepayPayment\Controller\Payment
 */
class Callback extends Action
{
    /**
     * @var LoggerAlepay
     */
    private $logger;

    /**
     * @var HelperAlepay
     */
    private $helperAlepay;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var OrderStatusHistoryRepositoryInterface
     */
    private $orderStatusHistoryRepository;

    /**
     * @var HistoryFactory
     */
    private $orderHistoryFactory;

    /**
     * @var BuilderInterface
     */
    private $transactionBuilder;

    /**
     * @var OrderPaymentRepositoryInterface
     */
    private $orderPaymentRepository;

    /**
     * @var TransactionRepositoryInterface
     */
    private $transactionRepository;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var Config
     */
    private $config;

    /**
     * Callback Constructor.
     *
     * @param Context $context
     * @param LoggerAlepay $logger
     * @param HelperAlepay $helperAlepay
     * @param Session $checkoutSession
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository
     * @param HistoryFactory $orderHistoryFactory
     * @param BuilderInterface $transactionBuilder
     * @param OrderPaymentRepositoryInterface $orderPaymentRepository
     * @param TransactionRepositoryInterface $transactionRepository
     * @param HelperData $helperData
     * @param Config $config
     */
    public function __construct(
        Context $context,
        LoggerAlepay $logger,
        HelperAlepay $helperAlepay,
        Session $checkoutSession,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository,
        HistoryFactory $orderHistoryFactory,
        BuilderInterface $transactionBuilder,
        OrderPaymentRepositoryInterface $orderPaymentRepository,
        TransactionRepositoryInterface $transactionRepository,
        HelperData $helperData,
        Config $config
    ) {
        parent::__construct($context);
        $this->logger           = $logger;
        $this->helperAlepay     = $helperAlepay;
        $this->checkoutSession  = $checkoutSession;
        $this->orderRepository  = $orderRepository;
        $this->searchCriteriaBuilder        = $searchCriteriaBuilder;
        $this->orderStatusHistoryRepository = $orderStatusHistoryRepository;
        $this->orderHistoryFactory          = $orderHistoryFactory;
        $this->transactionBuilder           = $transactionBuilder;
        $this->orderPaymentRepository       = $orderPaymentRepository;
        $this->transactionRepository        = $transactionRepository;
        $this->helperData                   = $helperData;
        $this->config = $config;
    }

    /**
     * @inheritdoc
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        // TODO: Implement callback() method.
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect     = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $error              = __('Invalid response.');
        $request            = $this->_request;
        $dataResponse       = $request->getParam('data');
        $checksumResponse   = $request->getParam('checksum');

        //Verify data from Alepay
        $verify = $this->helperAlepay->verifyResponseDataFromAlepay(base64_decode($dataResponse), $checksumResponse);

        if ($dataResponse && $checksumResponse && $verify) {
            //Decrypt data
            $data = $this->helperAlepay->decryptCallbackData($dataResponse, $this->config->getEncryptKey());
            $dataCallback       = json_decode($data);
            //Get transaction info
            $transactionInfo    = $this->helperAlepay->getTransactionInfoFromAlepay($dataCallback->data);
            //Log
            $this->logger->critical('TransactionInfo Decrypted: ', [$transactionInfo]);
            if ($transactionInfo) {
                try {
                    $transaction = json_decode($transactionInfo, true);
                    //Something went wrong while processing your order.
                    if (isset($transaction['status']) && '000' !== $transaction['status']) {
                        $this->messageManager->addErrorMessage(
                            __('Service Unavailable: %1', isset($transaction['message']) ? $transaction['message'] : 'Something went wrong while processing your order.')
                        );

                        return $resultRedirect->setPath('checkout/cart', ['_secure' => true]);
                    }

                    $orderId = $this->checkoutSession->getLastOrderId();
                    if (empty($orderId)) {
                        $incrementId = $transaction['orderCode'];
                        $order = $this->getOrderByIncrementId($incrementId);
                    } else {
                        $order = $this->orderRepository->get($orderId);
                    }
                    if ($order instanceof \Magento\Sales\Model\Order) {
                        $orderStatus = $order->getStatus();
                        $extOrderId = (empty($orderId)) ? $order->getId() : $orderId;
                        if ($orderStatus === \Magento\Sales\Model\Order::STATE_PROCESSING || $orderStatus === \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {
                            if ('000' === $transaction['status']) {
                                $order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
                                $this->createTransaction($order, $transaction);
                                $order->setExtOrderId($extOrderId);
                                $this->addOrderHistory($order, __('AlePay 2.0 Transaction Has Been Successful.'));

                                $path = 'checkout/onepage/success';
                            } else {
                                $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                                $this->createTransaction($order, $transaction);
                                $order->setExtOrderId($extOrderId);
                                $this->addOrderHistory($order, __('AlePay 2.0 Transaction Failed.'));

                                $path = 'checkout/onepage/failure';
                            }
                            $this->orderRepository->save($order);
                            $this->checkoutSession->setClearQuote(true);

                            return $resultRedirect->setPath($path, ['_secure' => true]);
                        }
                        //The order no longer exists.
                        $this->messageManager->addErrorMessage(
                            __('Service unavailable: Order already processed!')
                        );
                    } else {
                        //The order no longer exists.
                        $this->messageManager->addErrorMessage(
                            __('Service unavailable: The order no longer exists.')
                        );
                    }

                    return $resultRedirect->setPath('checkout/cart', ['_secure' => true]);
                } catch (\Exception $e) {
                    $this->logger->critical($e);
                    $this->messageManager->addExceptionMessage(
                        $e,
                        __('Service unavailable: Invalid TransactionInfo.')
                    );

                    return $resultRedirect->setPath('checkout/cart', ['_secure' => true]);
                }
            }
            $error = __('Invalid TransactionInfo.');
        }
        $this->messageManager->addErrorMessage(
            __('Service unavailable: %1', $error)
        );

        return $resultRedirect->setPath('checkout/cart', ['_secure' => true]);
    }

    /**
     * @param string $incrementId
     * @return bool|\Magento\Sales\Api\Data\OrderInterface
     */
    private function getOrderByIncrementId(string $incrementId)
    {
        $this->searchCriteriaBuilder->addFilter('increment_id', $incrementId);
        $order = $this->orderRepository->getList(
            $this->searchCriteriaBuilder->create()
        );
        $items = $order->getItems();
        if (!empty($items)) {
            foreach ($items as $item) {
                return $item;
            }
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param string $comment
     * @return bool
     */
    private function addOrderHistory($order, $comment)
    {
        $history = $this->orderHistoryFactory->create()
            ->setComment($comment)
            ->setEntityName('order')
            ->setOrder($order);
        if (\Magento\Sales\Model\Order::STATE_COMPLETE === $order->getStatus()) {
            $history->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
        } else {
            $history->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
        }
        try {
            $this->orderRepository->save($order);
            $this->orderStatusHistoryRepository->save($history);

            return true;
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order|null $order
     * @param array $paymentData
     * @return bool
     */
    private function createTransaction($order = null, $paymentData = array())
    {
        try {
            //Get payment from order
            /** @var \Magento\Sales\Model\Order\Payment $payment */
            $payment = $order->getPayment();
            $payment->setLastTransId($paymentData['transactionCode']);
            $payment->setTransactionId($paymentData['transactionCode']);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
            );
            $formattedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );

            $message = __('The authorized amount is %1.', $formattedPrice);
            //Get the object of builder class
            $trans = $this->transactionBuilder;
            /** @var \Magento\Sales\Model\Order\Payment\Transaction $transaction */
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentData['transactionCode'])
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                //Build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setParentTransactionId(null);
            $this->orderPaymentRepository->save($payment);
            $this->orderRepository->save($order);

            return  $this->transactionRepository->save($transaction)->getTransactionId();
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return false;
    }

}
