<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Gateway\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Encryption\EncryptorInterface;;

/**
 * Class Config
 * @package Rony\AlepayPayment\Gateway\Config
 */
class Config extends \Magento\Payment\Gateway\Config\Config
{
    const KEY_ENVIRONMENT   = 'environment';

    const KEY_ACTIVE        = 'active';

    const KEY_TOKEN_KEY     = 'token_key';

    const KEY_CHECKSUM_KEY  = 'checksum_key';

    const KEY_ENCRYPT_KEY   = 'encrypt_key';

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Alepay config constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encryptor
     * @param null|string $methodCode
     * @param string $pathPattern
     * @param Json|null $serializer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor,
        $methodCode = null,
        $pathPattern = self::DEFAULT_PATH_PATTERN,
        Json $serializer = null
    ) {
        parent::__construct($scopeConfig, $methodCode, $pathPattern);
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(Json::class);
        $this->encryptor = $encryptor;
    }

    /**
     * Gets value of configured environment.
     * Possible values: production or sandbox.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getEnvironment($storeId = null): string
    {
        return $this->getValue(self::KEY_ENVIRONMENT, $storeId);
    }

    /**
     * Gets Payment configuration status.
     *
     * @param int|null $storeId
     * @return bool
     */
    public function isActive($storeId = null)
    {
        return (bool) $this->getValue(self::KEY_ACTIVE, $storeId);
    }

    /**
     * @return string
     */
    public function getTokenKey(): string
    {
        $value = $this->getValue(Config::KEY_TOKEN_KEY);

        return $this->encryptor->decrypt($value);
    }

    /**
     * @return string
     */
    public function getChecksumKey(): string
    {
        $value = $this->getValue(Config::KEY_CHECKSUM_KEY);

        return $this->encryptor->decrypt($value);
    }

    /**
     * @return string
     */
    public function getEncryptKey(): string
    {
        $value = $this->getValue(Config::KEY_ENCRYPT_KEY);

        return $this->encryptor->decrypt($value);
    }

    /**
     * Returns gateway server name depending on environment
     *
     * @access public
     * @param int|null $storeId
     * @return string server domain name
     */
    public function serverName($storeId = null): string
    {
        $environment = $this->getEnvironment($storeId);
        switch($environment) {
            case 'production':
                $serverName = 'alepay.vn';
                break;
            case 'sandbox':
                $serverName = 'alepay-sandbox.nganluong.vn';
                break;
            default:
                $serverName = 'localhost';
                break;
        }

        return $serverName;
    }

    /**
     * Returns the base Alepay gateway URL based on config values
     *
     * @access public
     * @param int|null $storeId
     * @return string Alepay gateway URL
     */
    public function baseUrl($storeId = null)
    {
        return sprintf('%s://%s', 'https', $this->serverName($storeId));
    }

    /**
     * @param string $path
     * @param int|null $storeId
     * @return string
     */
    public function getRequestUrl(string $path = '', $storeId = null): string
    {
        if (empty($path)) {
            return $this->baseUrl($storeId);
        }

        return $this->baseUrl($storeId) . DIRECTORY_SEPARATOR . $path;
    }
}
