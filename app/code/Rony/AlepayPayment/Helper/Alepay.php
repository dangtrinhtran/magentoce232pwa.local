<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Rony\AlepayPayment\Logger\Logger as LoggerAlepay;
use phpseclib\Crypt\RSA;
use Rony\AlepayPayment\Gateway\Config\Config;
use Rony\AlepayPayment\Helper\Data as HelperData;

/**
 * Alepay default helper
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Alepay extends AbstractHelper
{
    const DEFAULT_ORDER_STATUS  = 'pending';

    const ALEPAY_REQUEST_ORDER          = 'checkout/v1/request-order';

    const ALEPAY_GET_TRANSACTION_INFO   = 'checkout/v1/get-transaction-info';

    /**
     * @var RSA
     */
    protected $cryptRsa;

    /**
     * @var LoggerAlepay
     */
    protected $logger;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var HelperData
     */
    protected $helperData;

    /**
     * Alepay Constructor.
     *
     * @param Context $context
     * @param RSA $cryptRsa
     * @param LoggerAlepay $logger
     * @param Config $config
     * @param HelperData $helperData
     */
    public function __construct(
        Context $context,
        RSA $cryptRsa,
        LoggerAlepay $logger,
        Config $config,
        HelperData $helperData
    ) {
        $this->cryptRsa = $cryptRsa;
        $this->logger   = $logger;
        $this->config   = $config;
        $this->helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * @param string $data
     * @param string|RSA|array $publicKey
     * @return string
     */
    public function encryptData($data, $publicKey): string
    {
        $rsa = $this->cryptRsa;
        $rsa->loadKey($publicKey);
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        $output = $rsa->encrypt($data);

        return base64_encode($output);
    }

    /**
     * @param string $data
     * @param string|RSA|array $publicKey
     * @return string
     */
    public function decryptData($data, $publicKey): string
    {
        $rsa = $this->cryptRsa;
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        $cipherText = base64_decode($data);
        $rsa->loadKey($publicKey);
        $output = $rsa->decrypt($cipherText);

        return $output;
    }

    /**
     * @param string $data
     * @param string $publicKey
     * @return string
     */
    public function decryptCallbackData($data, $publicKey): string
    {
        $decoded = base64_decode($data);

        return $this->decryptData($decoded, $publicKey);
    }

    /**
     * @param array $orderData
     * @return bool|mixed|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function processAlepayConnection($orderData)
    {
        $storeId    = $this->helperData->getStoreId();
        $requestUrl = $this->config->getRequestUrl(self::ALEPAY_REQUEST_ORDER, $storeId);
        $result     = $this->sendRequestToAlepay($orderData, $requestUrl);
        //Log
        $this->logger->critical('Response from Alepay: ', [$result]);

        if ($result && '000' === $result->errorCode) {
            $dataDecrypted = $this->decryptData($result->data, $this->config->getEncryptKey());

            return json_decode($dataDecrypted);
        }

        return $result;
    }

    /**
     * @param array $data
     * @param string $httpRequestUrl
     * @return bool|string
     */
    private function sendRequestToAlepay($data, $httpRequestUrl)
    {
        if (!empty($data) && is_array($data)){
            $dataEncrypted = $this->encryptData(json_encode($data), $this->config->getEncryptKey());
            $checksum = md5($dataEncrypted . $this->config->getChecksumKey());
            $dataRequest = array(
                'token'     => $this->config->getTokenKey(),
                'data'      => $dataEncrypted,
                'checksum'  => $checksum,
            );
            //Log
            $this->logger->critical('Data was encrypted that send to Alepay: ', $dataRequest);
            $dataRequestEncode = json_encode($dataRequest);
            //Initialize a cURL session
            $cUrlSession = curl_init($httpRequestUrl);
            curl_setopt($cUrlSession, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($cUrlSession, CURLOPT_POSTFIELDS, $dataRequestEncode);
            curl_setopt($cUrlSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cUrlSession, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($cUrlSession, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($dataRequestEncode))
            );
            $result = curl_exec($cUrlSession);
            //Close connection
            curl_close($cUrlSession);

            return json_decode($result);
        }

        return false;
    }

    /**
     * @param string $transactionCode
     * @return bool|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTransactionInfoFromAlepay($transactionCode)
    {
        $dataRequest    = ['transactionCode' => $transactionCode];
        $storeId        = $this->helperData->getStoreId();
        $requestUrl     = $this->config->getRequestUrl(self::ALEPAY_GET_TRANSACTION_INFO, $storeId);
        $response       = $this->sendRequestToAlepay($dataRequest, $requestUrl);
        //Log
        $this->logger->critical('Transaction Info from Alepay: ', [$response]);

        if ($response && '000' === $response->errorCode && $this->verifyResponseDataFromAlepay($response->data, $response->checksum)) {
            $dataDecrypted = $this->decryptData($response->data, $this->config->getEncryptKey());

            return  $dataDecrypted;
        } else {
            return false;
        }
    }

    /**
     * @param string $dataResponse
     * @param string $checksumResponse
     * @return bool
     */
    public function verifyResponseDataFromAlepay(string $dataResponse, string $checksumResponse): bool
    {
        $checksum = md5($dataResponse . $this->config->getChecksumKey());
        if ($checksumResponse === $checksum) {
            return true;
        }

        return false;
    }
}
