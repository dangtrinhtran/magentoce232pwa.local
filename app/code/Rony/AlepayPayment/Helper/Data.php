<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rony\AlepayPayment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Alepay default helper
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Data extends AbstractHelper
{
    /**
     * @var CountryInformationAcquirerInterface
     */
    private $countryInformationAcquirer;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Data Constructor.
     *
     * @param Context $context
     * @param CountryInformationAcquirerInterface $countryInformationAcquirer
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        CountryInformationAcquirerInterface $countryInformationAcquirer,
        StoreManagerInterface $storeManager
    ) {
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->storeManager     = $storeManager;
        parent::__construct($context);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = []): string
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }

    /**
     * @param string $countryCode
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCountryNameByCode($countryCode): string
    {
        $country = $this->countryInformationAcquirer->getCountryInfo($countryCode);
        if ($country) {
            return $country->getFullNameEnglish();
        }

        //Default is Vietnam
        return 'Viet Nam';
    }

    /**
     * @param array $street
     * @return string
     */
    public function getStreetToString(array $street): string
    {
        $streetString = '';
        if (empty($street)) {
            return $streetString;
        }
        $loop = 0;
        foreach ($street as $line) {
            $streetString .= ((0 < $loop) ? (' ' . trim($line)) : trim($line));
            $loop++;
        }

        return $streetString;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }
}
